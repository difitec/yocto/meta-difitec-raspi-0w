DESCRIPTION = "A base image for Raspberry-Pi-0-Wifi made by DIFITEC"
LICENSE = "MIT"

inherit core-image

require recipes-core/images/core-image-base.bb

IMAGE_INSTALL:append = " \
    dhcpcd \
    gdb gdbserver \
    i2c-tools \
    util-linux-agetty \
    "

# add a feature
EXTRA_IMAGE_FEATURES:append = " ssh-server-dropbear"

# add MQTT libraries
IMAGE_INSTALL:append = " libmosquitto1 libmosquittopp1"

# add iio libraries and tests
IMAGE_INSTALL:append = " libiio libiio-tests"

# add packages nano and mc
CORE_IMAGE_EXTRA_INSTALL:append = " nano mc"
