DESCRIPTION = "A minimal image for Raspberry-Pi-0-Wifi made by DIFITEC"
LICENSE = "MIT"

inherit core-image

require recipes-core/images/core-image-minimal.bb

IMAGE_INSTALL:append = " \
    gdb gdbserver \
    util-linux-agetty \
    "

# add a feature
EXTRA_IMAGE_FEATURES:append = " ssh-server-dropbear"

# add packages nano and mc
CORE_IMAGE_EXTRA_INSTALL:append = " nano mc"
