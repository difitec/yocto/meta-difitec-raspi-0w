# meta-difitec-raspi-0w

## Name

Yocto - Embedded Linux + Qt-Framework für Raspberry Zero Wifi

## Beschreibung

Diese Website enthält einen Yocto Meta Layer für eine konkrete Abbildung eines Embedded Linux auf einem Raspberry-Pi Zero Wifi.

Das Projekt dient hauptsächlich als Anleitung und Dokumentation für den generellen Umgang mit Yocto und dem Build System bitbake, erstellt anderseits jedoch auch ein lauffähiges System für den Raspberry-Pi Zero Wifi, welches für Projekte genutzt werden kann.

## Autor

Ulf Schönherr

Dieses Projekt wird bereitgestellt durch DIFITEC GmbH, Dresden, Germany.

## Lizenz

[MIT Lizenz](https://opensource.org/licenses/MIT)

Copyright 2023 DIFITEC GmbH

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Projekt Status

aktiv

## Image für den Raspberry-Pi Zero Wifi erstellen

### repo Tool installieren

```
mkdir ~/bin
export PATH=~/bin:$PATH # you can put this into ~/.bashrc
curl https://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo
```

Pfad in .bashrc anpassen:

```
export PATH=~/bin:$PATH
```

einmal ausloggen, einloggen

### Projekt	über repo klonen

```
mkdir -p ~/yocto
cd ~/yocto

repo init -u https://gitlab.com/difitec/yocto/repo-difitec-raspi-manifest.git -b main -m kirkstone-raspi-0w.xml
repo sync --no-clone-bundle
```

### Setup Environment

```
cd ~/yocto
. setup-environment
```

### Image erzeugen

```
bitbake difitec-raspi-0w-image-base
```

### SDK erzeugen

```
bitbake difitec-raspi-0w-image-base -c populate_sdk
```
